<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Product::latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn  = '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                    $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-danger btn-sm deleteProduct">Delete</a>';

                    return $btn;
                })
                ->rawColumns(["action"])
                ->make(true);

        }

        return view("product", compact("data"));
    }



    public function store(Request $request)
    {
        Product::updateOrCreate(["id" => $request->product_id],["name" => $request->name, "detail" => $request->detail]);

        return response()->json(["success"=> "Product Successfully Added!"]);
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return response()->json($product);
    }

    public function destroy($id)
    {
        Product::find($id)->delete();

        return response()->json(["Product Successfully Deleted!"]);
    }
}
